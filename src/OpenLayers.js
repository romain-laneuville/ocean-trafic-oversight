import {Map, View} from "ol";
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";

export class OpenLayers {
    constructor() {
        // Default properties
        this.map = new Map({
            target: 'map',
            layers: [new TileLayer({ source: new OSM() })],
            view  : this.defaultView()
        });
    }

    /**
     * Returns the default view center in Toulon
     *
     * @returns {View}
     */
    defaultView() {
        return new View ({
            center: [5.958047, 42.803046],
            zoom  : 8
        })
    }
}
