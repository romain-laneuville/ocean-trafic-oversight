const path = require('path');

module.exports = {
    output: {
        filename: 'main.js'
    },
    module: {
        rules: [
            { test: /\.css$/i, use: ['style-loader', 'css-loader'] },
            {
                test   : /\.js$/,
                exclude: /node_modules/,
                loader : 'babel-loader',
                options: {
                    presets: ['@babel/preset-env']
                }
            }
        ]
    }
};
